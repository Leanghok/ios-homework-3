//
//  PostWithStatus.swift
//  Homework_III
//
//  Created by Hour Leanghok on 12/4/18.
//  Copyright © 2018 Hour Leanghok. All rights reserved.
//

import UIKit

class PostWithStatus: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var shareImageView: UIImageView!
    
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var commentTextField: UITextField!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var localtionLabel: UILabel!
    
    @IBOutlet weak var likeCountLabel: UILabel!
    
    @IBOutlet weak var commentCountLabel: UILabel!
    
    @IBOutlet weak var shareCountLabel: UILabel!
    
    @IBOutlet weak var statusLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        profileImageView.layer.cornerRadius = profileImageView.frame.size.height / 2
        profileImageView.clipsToBounds = true
        
        shareImageView.layer.cornerRadius = profileImageView.frame.size.height / 2
        shareImageView.clipsToBounds = true
        
//        commentTextField.layer.cornerRadius = 15.0
//        commentTextField.layer.borderWidth = 2.0
//        commentTextField.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
