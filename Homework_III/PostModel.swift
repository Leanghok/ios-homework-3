//
//  PostModel.swift
//  Homework_III
//
//  Created by Hour Leanghok on 12/4/18.
//  Copyright © 2018 Hour Leanghok. All rights reserved.
//

import Foundation
import UIKit

class Post {
    var username: String
    var profileImage: UIImage
    var time: String
    var location: String
    var status: String
    var likeCount: String
    var commentCount: String
    var postImage: UIImage?
    var shareCount: String
    
    init(username:String, profileImage:UIImage, time:String, location:String, status:String, commentCount:String, postImgae:UIImage?, shareCount:String, likeCount:String){
        self.username = username
        self.profileImage = profileImage
        self.time = time
        self.location = location
        self.status = status
        self.likeCount = likeCount
        self.commentCount = commentCount
        self.postImage = postImgae
        self.shareCount = shareCount
    }
}
