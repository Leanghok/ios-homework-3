//
//  ViewController.swift
//  Homework_III
//
//  Created by Hour Leanghok on 12/4/18.
//  Copyright © 2018 Hour Leanghok. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var posts:[Post] = []
    
    @IBOutlet weak var postTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        postTableView.register(UINib(nibName: "PostWithStatus", bundle: nil), forCellReuseIdentifier: "PostWithStatusCell")
        postTableView.register(UINib(nibName: "PostWithStatusAndImage", bundle: nil), forCellReuseIdentifier: "PostWithStatusAndImage")
        
        posts.append(Post(username: "Tony Stark", profileImage: #imageLiteral(resourceName: "profile"), time: "Just Now", location: "New York", status: "You always need a Facebook status that everyone will like. Perfectly choosing the status for Facebook is really important to make a bang on your Facebook profile.", commentCount: "12", postImgae: #imageLiteral(resourceName: "iron-man"), shareCount: "100", likeCount: "10"))
        
        posts.append(Post(username: "Spider Man", profileImage: #imageLiteral(resourceName: "profile"), time: "4 hrs", location: "London", status: "Hey there!", commentCount: "20", postImgae: #imageLiteral(resourceName: "profile"), shareCount: "100", likeCount: "42"))
        
        posts.append(Post(username: "Tony Stark", profileImage: #imageLiteral(resourceName: "profile"), time: "Just Now", location: "New York", status: "You always need a Facebook status that everyone will like. Perfectly choosing the status for Facebook is really important to make a bang on your Facebook profile.", commentCount: "5", postImgae: nil, shareCount: "100", likeCount: "123"))
        
        posts.append(Post(username: "Captain Marvel", profileImage: #imageLiteral(resourceName: "captain marvel"), time: "4 hrs", location: "London", status: "Hey there!", commentCount: "6", postImgae: #imageLiteral(resourceName: "captain-marvel1"), shareCount: "100", likeCount: "111"))
        
        posts.append(Post(username: "Spider Man", profileImage: #imageLiteral(resourceName: "profile"), time: "4 hrs", location: "London", status: "Hey there!", commentCount: "9", postImgae: nil, shareCount: "100", likeCount: "12"))
        
        posts.append(Post(username: "Captain America", profileImage: #imageLiteral(resourceName: "captain-profile"), time: "4 hrs", location: "London", status: "Hey there!", commentCount: "111", postImgae: #imageLiteral(resourceName: "captain america"), shareCount: "100", likeCount: "61"))
        // Do any additional setup after loading the view, typically from a nib.

    }


}

extension ViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if posts[indexPath.row].postImage == nil {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PostWithStatusCell", for: indexPath) as! PostWithStatus
            cell.nameLabel.text = posts[indexPath.row].username
            cell.profileImageView.image = posts[indexPath.row].profileImage
            cell.timeLabel.text = (posts[indexPath.row].time + " ~ ")
            cell.localtionLabel.text = (posts[indexPath.row].location + " ~ ")
            cell.statusLabel.text = posts[indexPath.row].status
            cell.likeCountLabel.text = (posts[indexPath.row].likeCount + " Likes")
            cell.commentCountLabel.text = (posts[indexPath.row].commentCount + " Comments")
            cell.shareCountLabel.text = (posts[indexPath.row].shareCount + " Shares")
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PostWithStatusAndImage", for: indexPath) as! PostWithStatusAndImage
            cell.postImageView.image = posts[indexPath.row].postImage!
            cell.usernameLabel.text = posts[indexPath.row].username
            cell.profileImageView.image = posts[indexPath.row].profileImage
            cell.timeLabel.text = (posts[indexPath.row].time + " ~ ")
            cell.locationLabel.text = (posts[indexPath.row].location + " ~ ")
            cell.statusLabel.text = posts[indexPath.row].status
            cell.likeCountLabel.text = (posts[indexPath.row].likeCount + " Likes")
            cell.commentCountLabel.text = (posts[indexPath.row].commentCount + " Comments")
            cell.shareCountLabel.text = (posts[indexPath.row].shareCount + " Shares")
            return cell
        }
        
        
    }
    
    
}

extension ViewController : UITableViewDelegate{
    
}

